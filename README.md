# Lebenslauf

## `cv.tex`
Ein Lebenslauf basierend auf der Vorlage von [posquit0/Aweseome-CV](https://github.com/posquit0/Awesome-CV).  

## `coverletter.tex`
Ein Beispiel Motivationsschreiben, auch auf der selben Vorlage basierend  

## Compile
### macOS
1. Install `mactex`: `brew cask install mactex`  
2. use lualatex to compile  
(Missing modules installing via `sudo tlmgr install <module>`
